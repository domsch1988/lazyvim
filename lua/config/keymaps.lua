-- Keymaps are automatically loaded on the VeryLazy event
-- Default keymaps that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/keymaps.lua
-- Add any additional keymaps here
local map = LazyVim.safe_keymap_set

map("", "ö", ":", { desc = "German Colon Remap" })

-- Custom Functionality
-- Password Generator
map("n", "<leader>ip", function()
  local command = "pwgen -N 1 32"
  local password = vim.fn.systemlist(command)
  for _, line in ipairs(password) do
    vim.api.nvim_put({ line }, "", true, true)
  end
end, { noremap = true, silent = true, desc = "Insert Password" })
